'''
Make sure you uploaded "StandardFirmata" as sketch on the arduino!
'''
# A simple loop that reads values from the analog inputs of an Arduino port.
# No Arduino code is necessary - just upload the standard-firmata sketch from the examples.

import pyfirmata
import signal
import sys
import time
from tkinter import * 

# Erzeugung des Fensters
tkFenster = Tk()
tkFenster.title('Anzeige')
tkFenster.geometry("480x260")
# Aktivierung des Fensters
labelbeginone_left = Label(master=tkFenster, bg='red3')
labelbeginone_left.place(x=5, y=5, width=153, height=80)
labelbegintwo_left = Label(master=tkFenster, bg="gold2")
labelbegintwo_left.place(x=5, y=90, width=153, height=80)
labelbeginthree_left = Label(master=tkFenster, bg="green3")
labelbeginthree_left.place(x=5, y=175, width=153, height=80)
labelbeginone_center = Label(master=tkFenster, bg='red3')
labelbeginone_center.place(x=163, y=5, width=153, height=80)
labelbegintwo_center = Label(master=tkFenster, bg="gold2")
labelbegintwo_center.place(x=163, y=90, width=153, height=80)
labelbeginthree_center = Label(master=tkFenster, bg="green3")
labelbeginthree_center.place(x=163, y=175, width=153, height=80)
labelbeginone_right = Label(master=tkFenster, bg='red3')
labelbeginone_right.place(x=321, y=5, width=153, height=80)
labelbegintwo_right = Label(master=tkFenster, bg="gold2")
labelbegintwo_right.place(x=321, y=90, width=153, height=80)
labelbeginthree_right = Label(master=tkFenster, bg="green3")
labelbeginthree_right.place(x=321, y=175, width=153, height=80)

tkFenster.update()

# Definition of the analog pins you want to monitor e.g. (1,2,4)
PINS = [0, 1, 2]

# Do a graceful shutdown, otherwise the program will hang if you kill it.
def signal_handler(signal, frame):
    board.exit()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

def read_data(pin):
    #The data are output as floating point numbers
    analogout = board.analog[pin].read()
    #converts the output to cm (analogout * 5000, analogout * 0.0045)
    distance = analogout * 22.5 + 2.5
    return distance

# Connect to the board
print ("Setting up the connection to the board ...")
board = pyfirmata.Arduino('/dev/ttyACM0')

# Iterator thread is needed for correctly reading analog input
it = pyfirmata.util.Iterator(board)
it.start()
 
# Start reporting for defined pins
for pin in PINS:
    board.analog[pin].enable_reporting()
 
# Loop that keeps printing values
    labelread_left = Label(master=tkFenster, bg='gray', text = "Sensor 1")
    labelread_left.place(x=5, y=5, width=153, height=250)
    labelread_center = Label(master=tkFenster, bg='gray', text = "Sensor 2")
    labelread_center.place(x=163, y=5, width=153, height=250)
    labelread_right = Label(master=tkFenster, bg='gray', text = "Sensor 3")
    labelread_right.place(x=321, y=5, width=153, height=250)

print ("Connected")

while 1:
    # Daten auswerte Sensor 1
    data_zero = read_data(0)
    if data_zero != 25.00:
        labelread_left.config (bg="red3")
        tkFenster.update()
    else:
        labelread_left.config (bg="green3")
        tkFenster.update()
    
    # Daten auswerten Sensor 2
    data_one = read_data(1) 
    if data_one >= 9.00 and data_one <= 16.00:
        labelread_center.config(bg="gold2")
        tkFenster.update()
    elif data_one < 9.00:
        labelread_center.config(bg="red3")
        tkFenster.update()
    elif data_one > 16.00:
        labelread_center.config(bg="green3")
        tkFenster.update()

    # Daten auswerten Sensor 3
    data_two = read_data(2)
    if data_two <= 12:
        labelread_right.config(bg="red3")
        tkFenster.update()
    elif data_two > 12 and data_two <= 16:
        labelread_right.config(bg="gold2")
        tkFenster.update()
    elif data_two > 16:
        labelread_right.config(bg="green3")
        tkFenster.update()

    board.pass_time(1)





    
    

    
