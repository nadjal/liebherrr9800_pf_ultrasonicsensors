'''
Make sure you uploaded "StandardFirmata" as sketch on the arduino!
'''

# A simple loop that reads values from the analog inputs of an Arduino port.
# No Arduino code is necessary - just upload the standard-firmata sketch from the examples.

import pyfirmata
import signal
import sys
import time
from tkinter import *
import pandas as pd
import os

#define window dimensions (fits on 3.5 inch display of the Raspberry Pi) (480x250) 
canvas_width = 380
canvas_height = 250
line_width = 8

#creates the window with Tkinter
master = Tk()
master.geometry("480x250")
master.title("LR9800_Rear_View")

canvas = Canvas(master, width=canvas_width, height=canvas_height, bg="#014A7F")
canvas.pack(side="right")

#defines the image (must be located in the same folder as the program)
img = PhotoImage(file="bagger_top_rasp.png")
canvas.create_image(95,53, anchor=NW, image=img)

def exit_():
    os._exit(1)
    master.destroy()

label = Label(master, fg="dark green", text="Setting up \n connection\n")
label.pack()
button = Button(master, text='Exit', width=10, command=exit_)
button.pack()
label_one = Label(master, fg="black", text="S1_center: \n -- \n")
label_one.pack()
label_two = Label(master, fg="black", text="S2_(left): \n -- \n")
label_two.pack()
label_three = Label(master, fg="black", text="S3_(right): \n -- \n")
label_three.pack()

#coordinates (coords) = x0, y0, x1, y1

arccolor_0 = "#466378" #gray
arccolor_1 = "#B40404" #red
arccolor_2 = "#DF3A01" #red-orange
arccolor_3 = "#FF8000" #orange
arccolor_4 = "#FFFF00" #yellow
arccolor_5 = "#3ADF00" #green
arccolor_6 = "#8A0808" #darkred
bgcolor = "#014A7F" # blue

#creates five arcs for each sensor
#sensor 2 left from the back
s2_first_coords = 100, 50, 130, 80 
two_first = canvas.create_arc(s2_first_coords, start=55, extent=180, width=line_width, style = ARC, fill=arccolor_1, outline = arccolor_1)

s2_second_coords = 90, 40, 140, 90
two_second = canvas.create_arc(s2_second_coords, start=65, extent=160, width=line_width, style = ARC, fill=arccolor_2, outline = arccolor_2)

s2_third_coords = 80, 30, 150, 100
two_third = canvas.create_arc(s2_third_coords, start =75, extent = 140, width = line_width, style = ARC, fill=arccolor_3, outline = arccolor_3)

s2_fourth_coords = 70, 20, 160, 110
two_fourth = canvas.create_arc(s2_fourth_coords, start =85, extent = 120, width = line_width, style = ARC, fill=arccolor_4, outline = arccolor_4)

s2_fifth_coords = 60, 10, 170, 120
two_fifth = canvas.create_arc(s2_fifth_coords, start =95, extent = 100, width = line_width, style = ARC, fill=arccolor_5, outline = arccolor_5)

#sensor 3 right from the back
s3_first_coords = 100, 180, 130, 210
three_first = canvas.create_arc(s3_first_coords, start=125, extent=180, width=line_width, style = ARC, fill=arccolor_1, outline = arccolor_1)

s3_second_coords = 90, 170, 140, 220
three_second = canvas.create_arc(s3_second_coords, start=135, extent=160, width=line_width, style = ARC, fill=arccolor_2, outline = arccolor_2)

s3_third_coords = 80, 160, 150, 230
three_third = canvas.create_arc(s3_third_coords, start = 145, extent = 140, width = line_width, style = ARC, fill=arccolor_3, outline = arccolor_3)

s3_fourth_coords = 70, 150, 160, 240
three_fourth = canvas.create_arc(s3_fourth_coords, start = 155, extent = 120, width = line_width, style = ARC, fill=arccolor_4, outline = arccolor_4)

s3_fifth_coords = 60, 140, 170, 250
three_fifth = canvas.create_arc(s3_fifth_coords, start = 165, extent = 100, width = line_width, style = ARC, fill=arccolor_5, outline = arccolor_5)

#sensor 1 center
s1_first_coords = 100, 115, 130, 145 
one_first = canvas.create_arc(s1_first_coords, start=90, extent=180, width=line_width, style = ARC, fill=arccolor_1, outline = arccolor_1)

s1_second_coords = 90, 105, 140, 155
one_second = canvas.create_arc(s1_second_coords, start=100, extent=160, width=line_width, style = ARC, fill = arccolor_2, outline = arccolor_2)

s1_third_coords = 80, 95, 150, 165
one_third = canvas.create_arc(s1_third_coords, start = 110, extent = 140, width = line_width, style = ARC, fill=arccolor_3, outline = arccolor_3)

s1_fourth_coords = 70, 85, 160, 175
one_fourth = canvas.create_arc(s1_fourth_coords, start = 120, extent = 120, width = line_width, style = ARC, fill=arccolor_4, outline = arccolor_4)

s1_fifth_coords = 60, 75, 170, 185
one_fifth = canvas.create_arc(s1_fifth_coords, start = 130, extent = 100, width = line_width, style = ARC, fill=arccolor_5, outline = arccolor_5)

master.update()

# Definition of the analog pins
PINS = [0, 1, 2]

# Do a graceful shutdown (press Ctrl + C), otherwise the program will hang if you kill it.
def signal_handler(signal, frame):
    board.exit()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

#Reads the analog data on the Arduino and returns the distance in cm
def read_data(pin):
    #The data are output as floating point numbers
    analogout = board.analog[pin].read()
    #converts the output to cm (analogout * 5000, analogout * 0.0045)
    distance = analogout * 22.5 + 2.5
    return distance

# Connect to the board via USB
print ("Setting up the connection to the board ...")
board = pyfirmata.Arduino('/dev/ttyACM0')

# Iterator thread is needed for correctly reading analog input
it = pyfirmata.util.Iterator(board)
it.start()
 
# Start reporting for defined pins
for pin in PINS:
    board.analog[pin].enable_reporting()

print ("Connected")
label.config(text = "Connected")
canvas.itemconfig(one_first, outline = arccolor_0)
canvas.itemconfig(one_second, outline = arccolor_0)
canvas.itemconfig(one_third, outline = arccolor_0)
canvas.itemconfig(one_fourth, outline = arccolor_0)
canvas.itemconfig(one_fifth, outline = arccolor_0)

canvas.itemconfig(two_first, outline = arccolor_0)
canvas.itemconfig(two_second, outline = arccolor_0)
canvas.itemconfig(two_third, outline = arccolor_0)
canvas.itemconfig(two_fourth, outline = arccolor_0)
canvas.itemconfig(two_fifth, outline = arccolor_0)
            
canvas.itemconfig(three_first, outline = arccolor_0)
canvas.itemconfig(three_second, outline = arccolor_0)
canvas.itemconfig(three_third, outline = arccolor_0)
canvas.itemconfig(three_fourth, outline = arccolor_0)
canvas.itemconfig(three_fifth, outline = arccolor_0)
                        
master.update()

# Definitions for dataframe
columnnames = ['Sensor_1', 'Sensor_2', 'Sensor_3']
counter = 0
sencounter = 0
sencounter_2 = 0

# create dataframe
data = {columnnames[0]: [0, 25], columnnames[1]: [0, 25], columnnames[2]: [0, 25]}
df_marks = pd.DataFrame(data)

blindrange_one = False
blindrange_two = False
blindrange_three = False

time.sleep(0.2)

while True:
    #Each variable is assigned the output of the appropriate sensor
    data_one = read_data(0)
    data_two = read_data(1)
    data_three = read_data(2)

    label_one.config(text="S1_center: \n"+ str(round(data_one, 2))+ " cm\n")
    label_two.config(text="S2_left: \n"+ str(round(data_two, 2))+ " cm \n")
    label_three.config(text="S3_right: \n"+ str(round(data_three, 2))+ " cm \n")
    if counter == 2:
        counter = 0
    
    df_marks.loc[counter, columnnames[0]] = data_one
    df_marks.loc[counter, columnnames[1]] = data_two
    df_marks.loc[counter, columnnames[2]] = data_three

    # Position of the last two measured values in the list
    sencounter = counter
    sencounter_2 = counter-1
    # necessary, because there is no value at position -1
    if counter == 0:
        sencounter = counter
        sencounter_2 = 1
    
#Sensor data 1 are processed and graphically displayed
    if data_one == 25.00:
        # Writes content of the first column in the list
        sensorone_fix =  [i for i in df_marks.iloc[0:2, 0]]
        # Checks the last measured value and conclude whether it is the blind range or not 
        if sensorone_fix[sencounter] != sensorone_fix[sencounter_2]:
            #blind range
            if sensorone_fix[sencounter_2] <= 8:
                canvas.itemconfig(one_first, outline = arccolor_6)
                canvas.itemconfig(one_second, outline = arccolor_0)
                canvas.itemconfig(one_third, outline = arccolor_0)
                canvas.itemconfig(one_fourth, outline = arccolor_0)
                canvas.itemconfig(one_fifth, outline = arccolor_0)
                blindrange_one = True
            if sensorone_fix[sencounter_2] > 8:
                canvas.itemconfig(one_first, outline = arccolor_0)
                canvas.itemconfig(one_second, outline = arccolor_0)
                canvas.itemconfig(one_third, outline = arccolor_0)
                canvas.itemconfig(one_fourth, outline = arccolor_0)
                canvas.itemconfig(one_fifth, outline = arccolor_0)
                blindrange_one = False
            master.update()
        if sensorone_fix[sencounter] == sensorone_fix[sencounter_2]:
            if blindrange_one == True:
                label_one.config(text="S1_center: \n < 2.50 cm \n")
            else:
                label_one.config(text="S1_center: \n 25.00 cm \n")
            master.update()
    elif data_one < 25.00 and data_one > 20.00:
        canvas.itemconfig(one_first, outline = arccolor_0)
        canvas.itemconfig(one_second, outline = arccolor_0)
        canvas.itemconfig(one_third, outline = arccolor_0)
        canvas.itemconfig(one_fourth, outline = arccolor_0)
        canvas.itemconfig(one_fifth, outline = arccolor_5)
        master.update()
    elif data_one <= 20.00 and data_one > 15.00:
        canvas.itemconfig(one_first, outline = arccolor_0)
        canvas.itemconfig(one_second, outline = arccolor_0)
        canvas.itemconfig(one_third, outline = arccolor_0)
        canvas.itemconfig(one_fourth, outline = arccolor_4)
        canvas.itemconfig(one_fifth, outline = arccolor_0)
        master.update()
    elif data_one <= 15.00 and data_one > 10.00:
        canvas.itemconfig(one_first, outline = arccolor_0)
        canvas.itemconfig(one_second, outline = arccolor_0)
        canvas.itemconfig(one_third, outline = arccolor_3)
        canvas.itemconfig(one_fourth, outline = arccolor_0)
        canvas.itemconfig(one_fifth, outline = arccolor_0)
        master.update()
    elif data_one <= 10.00 and data_one > 7.00:
        canvas.itemconfig(one_first, outline = arccolor_0)
        canvas.itemconfig(one_second, outline = arccolor_2)
        canvas.itemconfig(one_third, outline = arccolor_0)
        canvas.itemconfig(one_fourth, outline = arccolor_0)
        canvas.itemconfig(one_fifth, outline = arccolor_0)
        master.update()
    elif data_one <= 7.00 and data_one > 5.00:
        canvas.itemconfig(one_first, outline = arccolor_1)
        canvas.itemconfig(one_second, outline = arccolor_0)
        canvas.itemconfig(one_third, outline = arccolor_0)
        canvas.itemconfig(one_fourth, outline = arccolor_0)
        canvas.itemconfig(one_fifth, outline = arccolor_0)
        master.update()
    elif (data_one < 5.00): 
        canvas.itemconfig(one_first, outline = arccolor_6)
        canvas.itemconfig(one_second, outline = arccolor_0)
        canvas.itemconfig(one_third, outline = arccolor_0)
        canvas.itemconfig(one_fourth, outline = arccolor_0)
        canvas.itemconfig(one_fifth, outline = arccolor_0)
        master.update()

#Sensor data 2 are processed and graphically displayed
    if data_two == 25.00:
        sensortwo_fix =  [i for i in df_marks.iloc[0:2, 1]]
        # Checks the last measured value and conclude whether it is the blind range or not 
        if sensortwo_fix[sencounter] != sensortwo_fix[sencounter_2]:
            if sensortwo_fix[sencounter_2] <= 8:
                canvas.itemconfig(two_first, outline = arccolor_6)
                canvas.itemconfig(two_second, outline = arccolor_0)
                canvas.itemconfig(two_third, outline = arccolor_0)
                canvas.itemconfig(two_fourth, outline = arccolor_0)
                canvas.itemconfig(two_fifth, outline = arccolor_0)
                blindrange_two = True 
            if sensortwo_fix[sencounter_2] > 8:
                canvas.itemconfig(two_first, outline = arccolor_0)
                canvas.itemconfig(two_second, outline = arccolor_0)
                canvas.itemconfig(two_third, outline = arccolor_0)
                canvas.itemconfig(two_fourth, outline = arccolor_0)
                canvas.itemconfig(two_fifth, outline = arccolor_0)
                blindrange_two = False
            master.update()
        if sensortwo_fix[sencounter] == sensortwo_fix[sencounter_2]:
            if blindrange_two == True:
                label_two.config(text="S2_left: \n < 2.50 cm \n")
            else:
                label_two.config(text="S2_left: \n 25.00 cm \n")
            master.update()
    elif data_two < 25.00 and data_two > 20.00:
        canvas.itemconfig(two_first, outline = arccolor_0)
        canvas.itemconfig(two_second, outline = arccolor_0)
        canvas.itemconfig(two_third, outline = arccolor_0)
        canvas.itemconfig(two_fourth, outline = arccolor_0)
        canvas.itemconfig(two_fifth, outline = arccolor_5)
        master.update()
    elif data_two <= 20.00 and data_two > 15.00:
        canvas.itemconfig(two_first, outline = arccolor_0)
        canvas.itemconfig(two_second, outline = arccolor_0)
        canvas.itemconfig(two_third, outline = arccolor_0)
        canvas.itemconfig(two_fourth, outline = arccolor_4)
        canvas.itemconfig(two_fifth, outline = arccolor_0)
        master.update()
    elif data_two <= 15.00 and data_two > 10.00:
        canvas.itemconfig(two_first, outline = arccolor_0)
        canvas.itemconfig(two_second, outline = arccolor_0)
        canvas.itemconfig(two_third, outline = arccolor_3)
        canvas.itemconfig(two_fourth, outline = arccolor_0)
        canvas.itemconfig(two_fifth, outline = arccolor_0)
        master.update()
    elif data_two <= 10.00 and data_two > 7.00:
        canvas.itemconfig(two_first, outline = arccolor_0)
        canvas.itemconfig(two_second, outline = arccolor_2)
        canvas.itemconfig(two_third, outline = arccolor_0)
        canvas.itemconfig(two_fourth, outline = arccolor_0)
        canvas.itemconfig(two_fifth, outline = arccolor_0)
        master.update()
    elif data_two <= 7.00 and data_two > 5.00:
        canvas.itemconfig(two_first, outline = arccolor_1)
        canvas.itemconfig(two_second, outline = arccolor_0)
        canvas.itemconfig(two_third, outline = arccolor_0)
        canvas.itemconfig(two_fourth, outline = arccolor_0)
        canvas.itemconfig(two_fifth, outline = arccolor_0)
        master.update()
    elif (data_two < 5.00): 
        canvas.itemconfig(two_first, outline = arccolor_6)
        canvas.itemconfig(two_second, outline = arccolor_0)
        canvas.itemconfig(two_third, outline = arccolor_0)
        canvas.itemconfig(two_fourth, outline = arccolor_0)
        canvas.itemconfig(two_fifth, outline = arccolor_0)
        master.update()

#Sensor data 3 are processed and graphically displayed
    if data_three == 25.00:
        sensorthree_fix =  [i for i in df_marks.iloc[0:2, 2]]
        # Checks the last measured value and conclude whether it is the blind range or not 
        if sensorthree_fix[sencounter] != sensorthree_fix[sencounter_2]:
            if sensorthree_fix[sencounter_2] <= 8:
                canvas.itemconfig(three_first, outline = arccolor_6)
                canvas.itemconfig(three_second, outline = arccolor_0)
                canvas.itemconfig(three_third, outline = arccolor_0)
                canvas.itemconfig(three_fourth, outline = arccolor_0)
                canvas.itemconfig(three_fifth, outline = arccolor_0)
                blindrange_three= True 
            if sensorthree_fix[sencounter_2] > 8:
                canvas.itemconfig(three_first, outline = arccolor_0)
                canvas.itemconfig(three_second, outline = arccolor_0)
                canvas.itemconfig(three_third, outline = arccolor_0)
                canvas.itemconfig(three_fourth, outline = arccolor_0)
                canvas.itemconfig(three_fifth, outline = arccolor_0)
                blindrange_three = False 
            master.update()
        if sensorthree_fix[sencounter] == sensorthree_fix[sencounter_2]:
            if blindrange_three == True:
                label_three.config(text="S3_right: \n < 2.50 cm \n")
            else:
                label_three.config(text="S3_right: \n 25.00 cm \n")
            master.update()
    elif data_three < 25.00 and data_three > 20.00:
        canvas.itemconfig(three_first, outline = arccolor_0)
        canvas.itemconfig(three_second, outline = arccolor_0)
        canvas.itemconfig(three_third, outline = arccolor_0)
        canvas.itemconfig(three_fourth, outline = arccolor_0)
        canvas.itemconfig(three_fifth, outline = arccolor_5)
        master.update()
    elif data_three <= 20.00 and data_three > 15.00:
        canvas.itemconfig(three_first, outline = arccolor_0)
        canvas.itemconfig(three_second, outline = arccolor_0)
        canvas.itemconfig(three_third, outline = arccolor_0)
        canvas.itemconfig(three_fourth, outline = arccolor_4)
        canvas.itemconfig(three_fifth, outline = arccolor_0)
        master.update()
    elif data_three <= 15.00 and data_three > 10.00:
        canvas.itemconfig(three_first, outline = arccolor_0)
        canvas.itemconfig(three_second, outline = arccolor_0)
        canvas.itemconfig(three_third, outline = arccolor_3)
        canvas.itemconfig(three_fourth, outline = arccolor_0)
        canvas.itemconfig(three_fifth, outline = arccolor_0)
        master.update()
    elif data_three <= 10.00 and data_three > 7.00:
        canvas.itemconfig(three_first, outline = arccolor_0)
        canvas.itemconfig(three_second, outline = arccolor_2)
        canvas.itemconfig(three_third, outline = arccolor_0)
        canvas.itemconfig(three_fourth, outline = arccolor_0)
        canvas.itemconfig(three_fifth, outline = arccolor_0)
        master.update()
    elif data_three <= 7.00 and data_three > 5.00:
        canvas.itemconfig(three_first, outline = arccolor_1)
        canvas.itemconfig(three_second, outline = arccolor_0)
        canvas.itemconfig(three_third, outline = arccolor_0)
        canvas.itemconfig(three_fourth, outline = arccolor_0)
        canvas.itemconfig(three_fifth, outline = arccolor_0)
        master.update()
    elif (data_three < 5.00): 
        canvas.itemconfig(three_first, outline = arccolor_6)
        canvas.itemconfig(three_second, outline = arccolor_0)
        canvas.itemconfig(three_third, outline = arccolor_0)
        canvas.itemconfig(three_fourth, outline = arccolor_0)
        canvas.itemconfig(three_fifth, outline = arccolor_0)
        master.update()

    counter = counter + 1
    board.pass_time(0.025)
