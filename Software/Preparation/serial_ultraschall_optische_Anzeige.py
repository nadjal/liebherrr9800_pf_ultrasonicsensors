import serial
import time
from tkinter import *

# Erzeugung des Fensters
tkFenster = Tk()
tkFenster.title('Anzeige')
tkFenster.geometry("360x410")
# Aktivierung des Fensters
labelbeginone = Label(master=tkFenster, bg='red3')
labelbeginone.place(x=5, y=5, width=350, height=130)
labelbegintwo = Label(master=tkFenster, bg="gold2")
labelbegintwo.place(x=5, y=140, width=350, height=130)
labelbeginthree = Label(master=tkFenster, bg="green3")
labelbeginthree.place(x=5, y=275, width=350, height=130)
tkFenster.update()

port = serial.Serial("/dev/ttyS0", baudrate=19200,timeout=3.0)
factoryreset = [0xA7, 0x36, 0x55, 0x4F]
port.write(factoryreset)
for i in range(6):
    resetresponce = port.read(1).hex()
    #print ("Reset", resetresponce)
adressrequest = [0xA8, 0x00, 0x00, 0x43]
port.write(adressrequest)
for i in range(6):
    adressresponse = port.read(1).hex()
    #print ("Adress: ", adressresponse)
temprequest = [0xA7, 0x0A, 0x01, 0x51]
port.write(temprequest)
for i in range(6):
    tempresponse = port.read(1).hex()
    #print ("Temp", tempresponse)
    
while True:
    labelread = Label(master=tkFenster, bg='gray')
    labelread.place(x=5, y=5, width=350, height=400)
    time.sleep(0.5)
    distancerequest = [0xAF, 0xFC, 0xFE, 0x40]
    port.write(distancerequest)
    time.sleep(0.55)
    for i in range(6):
        responce = int(port.read(1).hex(), 16)
        if i == 4:
            distance = responce
            print ("Distance:", responce)
    if distance > 1 and distance<=20:
        print("RED")
        labelread.config(bg="red3")
        tkFenster.update()
    elif distance > 20 and distance <= 60:
        print("ORANGE")
        labelread.config(bg="gold2")
        tkFenster.update()
    elif distance > 60 and distance <=150:
        print("GREEN")
        labelread.config(bg="green3")
        tkFenster.update()

    elif distance == 1: 
        print("BLINDZONE")
        labelread.config(bg="gray")
        tkFenster.update()

    elif distance == 0:
        print("nothing detected")
        labelread.config(bg="black")
        tkFenster.update()
        

