from tkinter import *
import RPi.GPIO as GPIO
import time

# Erzeugung des Fensters
tkFenster = Tk()
tkFenster.title('Anzeige')
tkFenster.geometry("360x410")
# Aktivierung des Fensters
labelbeginone = Label(master=tkFenster, bg='red3')
labelbeginone.place(x=5, y=5, width=350, height=130)
labelbegintwo = Label(master=tkFenster, bg="gold2")
labelbegintwo.place(x=5, y=140, width=350, height=130)
labelbeginthree = Label(master=tkFenster, bg="green3")
labelbeginthree.place(x=5, y=275, width=350, height=130)
tkFenster.update()

channel= 11
GPIO.setmode(GPIO.BOARD)

GPIO.setup(channel, GPIO.IN)

while True:
    labelread = Label(master=tkFenster, bg='gray')
    labelread.place(x=5, y=5, width=350, height=400)
    time.sleep(1)
    if GPIO.input(channel) == 0:
        print("nichts detektiert")
        labelread.config(bg="red3")
        tkFenster.update()
    if GPIO.input(channel) == 1:
        print("detektiert")
        labelread.config(bg="green3")
        tkFenster.update()
        
