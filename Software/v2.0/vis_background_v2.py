# A simple loop that reads values from the analog inputs of an Arduino port.
# No Arduino code is necessary - just upload the standard-firmata sketch from the examples.

import pyfirmata
import signal
import sys
import time
from tkinter import * 

#Fenstereinstellungen 
canvas_width = 480
canvas_height = 250
line_width = 8


master = Tk()

canvas = Canvas(master, 
           width=canvas_width, 
           height=canvas_height)
canvas.pack()

img = PhotoImage(file="bagger_top_rasp.png")
canvas.create_image(180,53, anchor=NW, image=img)

# coord = x0, y0, x1, y1

#für jeden Sensor erst mal drei arcs
#Sensor 2 (links/oben)
s2_first_coords = 160, 50, 190, 80 
two_first = canvas.create_arc(s2_first_coords, start=55, extent=180, width=line_width, style = ARC)

s2_second_coords = 150, 40, 200, 90
two_second = canvas.create_arc(s2_second_coords, start=65, extent=160, width=line_width, style = ARC)

s2_third_coords = 140, 30, 210, 100
two_third = canvas.create_arc(s2_third_coords, start =75, extent = 140, width = line_width, style = ARC)

s2_fourth_coords = 130, 20, 220, 110
two_fourth = canvas.create_arc(s2_fourth_coords, start =85, extent = 120, width = line_width, style = ARC)

s2_fifth_coords = 120, 10, 230, 120
two_fifth = canvas.create_arc(s2_fifth_coords, start =95, extent = 100, width = line_width, style = ARC)


#Sensor 3 (rechts/unten)
s3_first_coords = 160, 180, 190, 210
three_first = canvas.create_arc(s3_first_coords, start=125, extent=180, width=line_width, style = ARC)

s3_second_coords = 150, 170, 200, 220
three_second = canvas.create_arc(s3_second_coords, start=135, extent=160, width=line_width, style = ARC)

s3_third_coords = 140, 160, 210, 230
three_third = canvas.create_arc(s3_third_coords, start = 145, extent = 140, width = line_width, style = ARC)

s3_fourth_coords = 130, 150, 220, 240
three_fourth = canvas.create_arc(s3_fourth_coords, start = 155, extent = 120, width = line_width, style = ARC)

s3_fifth_coords = 120, 140, 230, 250
three_fifth = canvas.create_arc(s3_fifth_coords, start = 165, extent = 100, width = line_width, style = ARC)


#Sensor 1 (Mitte)
s1_first_coords = 160, 115, 190, 145 
one_first = canvas.create_arc(s1_first_coords, start=90, extent=180, width=line_width, style = ARC)

s1_second_coords = 150, 105, 200, 155
one_second = canvas.create_arc(s1_second_coords, start=100, extent=160, width=line_width, style = ARC)

s1_third_coords = 140, 95, 210, 165
one_third = canvas.create_arc(s1_third_coords, start = 110, extent = 140, width = line_width, style = ARC)

s1_fourth_coords = 130, 85, 220, 175
one_fourth = canvas.create_arc(s1_fourth_coords, start = 120, extent = 120, width = line_width, style = ARC)

s1_fifth_coords = 120, 75, 230, 185
one_fifth = canvas.create_arc(s1_fifth_coords, start = 130, extent = 100, width = line_width, style = ARC)

#master.update()

# Definition of the analog pins you want to monitor e.g. (1,2,4)
PINS = [0, 1, 2]

# Do a graceful shutdown, otherwise the program will hang if you kill it.
def signal_handler(signal, frame):
    board.exit()
    sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

def read_data(pin):
    #print ("Pin %i : %s" % (pin+1, board.analog[pin].read()))

    analogout = float (str(board.analog[pin].read()))
    analogout = analogout * 5000
    #print (analogout)
    entf = analogout * 0.0045 + 2.5
    #print (round (entf, 2), "cm")
    return entf

# Connect to the board
print ("Setting up the connection to the board ...")
board = pyfirmata.Arduino('/dev/ttyACM0')

# Iterator thread is needed for correctly reading analog input
it = pyfirmata.util.Iterator(board)
it.start()
 
# Start reporting for defined pins
for pin in PINS:
    board.analog[pin].enable_reporting()

print ("Connected")
time.sleep(0.2)
master.update()

while 1:    
    data_one = read_data(0)
    data_two = read_data(1)
    data_three = read_data(2)

# Daten Sensor 1 verarbeiten 
    if data_one == 25.00:
        canvas.itemconfig(one_first, outline = "black")
        canvas.itemconfig(one_second, outline = "black")
        canvas.itemconfig(one_third, outline = "black")
        canvas.itemconfig(one_fourth, outline = "black")
        canvas.itemconfig(one_fifth, outline = "black")
        master.update()
    elif data_one < 25.00 and data_one > 20.00:
        canvas.itemconfig(one_first, outline = "black")
        canvas.itemconfig(one_second, outline = "black")
        canvas.itemconfig(one_third, outline = "black")
        canvas.itemconfig(one_fourth, outline = "black")
        canvas.itemconfig(one_fifth, outline = "gold2")
        master.update()
    elif data_one <= 20.00 and data_one > 15.00:
        canvas.itemconfig(one_first, outline = "black")
        canvas.itemconfig(one_second, outline = "black")
        canvas.itemconfig(one_third, outline = "black")
        canvas.itemconfig(one_fourth, outline = "gold2")
        canvas.itemconfig(one_fifth, outline = "gold2")
        master.update()
    elif data_one <= 15.00 and data_one > 10.00:
        canvas.itemconfig(one_first, outline = "black")
        canvas.itemconfig(one_second, outline = "black")
        canvas.itemconfig(one_third, outline = "gold2")
        canvas.itemconfig(one_fourth, outline = "gold2")
        canvas.itemconfig(one_fifth, outline = "gold2")
        master.update()
    elif data_one <= 10.00 and data_one > 7.00:
        canvas.itemconfig(one_first, outline = "black")
        canvas.itemconfig(one_second, outline = "gold2")
        canvas.itemconfig(one_third, outline = "gold2")
        canvas.itemconfig(one_fourth, outline = "gold2")
        canvas.itemconfig(one_fifth, outline = "gold2")
        master.update()
    elif data_one <= 7.00 and data_one > 5.00:
        canvas.itemconfig(one_first, outline = "gold2")
        canvas.itemconfig(one_second, outline = "gold2")
        canvas.itemconfig(one_third, outline = "gold2")
        canvas.itemconfig(one_fourth, outline = "gold2")
        canvas.itemconfig(one_fifth, outline = "gold2")
        master.update()

    elif (data_one < 5.00): 
        canvas.itemconfig(one_first, outline = "red3")
        canvas.itemconfig(one_second, outline = "red3")
        canvas.itemconfig(one_third, outline = "red3")
        canvas.itemconfig(one_fourth, outline = "red3")
        canvas.itemconfig(one_fifth, outline = "red3")
        master.update()

# Daten Sensor 2 verarbeiten
    if data_two == 25.00:
        canvas.itemconfig(two_first, outline = "black")
        canvas.itemconfig(two_second, outline = "black")
        canvas.itemconfig(two_third, outline = "black")
        canvas.itemconfig(two_fourth, outline = "black")
        canvas.itemconfig(two_fifth, outline = "black")
        master.update()
    elif data_two < 25.00 and data_two > 20.00:
        canvas.itemconfig(two_first, outline = "black")
        canvas.itemconfig(two_second, outline = "black")
        canvas.itemconfig(two_third, outline = "black")
        canvas.itemconfig(two_fourth, outline = "black")
        canvas.itemconfig(two_fifth, outline = "gold2")
        master.update()
    elif data_two <= 20.00 and data_two > 15.00:
        canvas.itemconfig(two_first, outline = "black")
        canvas.itemconfig(two_second, outline = "black")
        canvas.itemconfig(two_third, outline = "black")
        canvas.itemconfig(two_fourth, outline = "gold2")
        canvas.itemconfig(two_fifth, outline = "gold2")
        master.update()
    elif data_two <= 15.00 and data_two > 10.00:
        canvas.itemconfig(two_first, outline = "black")
        canvas.itemconfig(two_second, outline = "black")
        canvas.itemconfig(two_third, outline = "gold2")
        canvas.itemconfig(two_fourth, outline = "gold2")
        canvas.itemconfig(two_fifth, outline = "gold2")
        master.update()
    elif data_two <= 10.00 and data_two > 7.00:
        canvas.itemconfig(two_first, outline = "black")
        canvas.itemconfig(two_second, outline = "gold2")
        canvas.itemconfig(two_third, outline = "gold2")
        canvas.itemconfig(two_fourth, outline = "gold2")
        canvas.itemconfig(two_fifth, outline = "gold2")
        master.update()
    elif data_two <= 7.00 and data_two > 5.00:
        canvas.itemconfig(two_first, outline = "gold2")
        canvas.itemconfig(two_second, outline = "gold2")
        canvas.itemconfig(two_third, outline = "gold2")
        canvas.itemconfig(two_fourth, outline = "gold2")
        canvas.itemconfig(two_fifth, outline = "gold2")
        master.update()
    elif (data_two < 5.00): 
        canvas.itemconfig(two_first, outline = "red3")
        canvas.itemconfig(two_second, outline = "red3")
        canvas.itemconfig(two_third, outline = "red3")
        canvas.itemconfig(two_fourth, outline = "red3")
        canvas.itemconfig(two_fifth, outline = "red3")
        master.update()

# Daten Sensor 3 verarbeiten
    if data_three == 25.00:
        canvas.itemconfig(three_first, outline = "black")
        canvas.itemconfig(three_second, outline = "black")
        canvas.itemconfig(three_third, outline = "black")
        canvas.itemconfig(three_fourth, outline = "black")
        canvas.itemconfig(three_fifth, outline = "black")
        master.update()
    elif data_three < 25.00 and data_three > 20.00:
        canvas.itemconfig(three_first, outline = "black")
        canvas.itemconfig(three_second, outline = "black")
        canvas.itemconfig(three_third, outline = "black")
        canvas.itemconfig(three_fourth, outline = "black")
        canvas.itemconfig(three_fifth, outline = "gold2")
        master.update()
    elif data_three <= 20.00 and data_three > 15.00:
        canvas.itemconfig(three_first, outline = "black")
        canvas.itemconfig(three_second, outline = "black")
        canvas.itemconfig(three_third, outline = "black")
        canvas.itemconfig(three_fourth, outline = "gold2")
        canvas.itemconfig(three_fifth, outline = "gold2")
        master.update()
    elif data_three <= 15.00 and data_three > 10.00:
        canvas.itemconfig(three_first, outline = "black")
        canvas.itemconfig(three_second, outline = "black")
        canvas.itemconfig(three_third, outline = "gold2")
        canvas.itemconfig(three_fourth, outline = "gold2")
        canvas.itemconfig(three_fifth, outline = "gold2")
        master.update()
    elif data_three <= 10.00 and data_three > 7.00:
        canvas.itemconfig(three_first, outline = "black")
        canvas.itemconfig(three_second, outline = "gold2")
        canvas.itemconfig(three_third, outline = "gold2")
        canvas.itemconfig(three_fourth, outline = "gold2")
        canvas.itemconfig(three_fifth, outline = "gold2")
        master.update()
    elif data_three <= 7.00 and data_three > 5.00:
        canvas.itemconfig(three_first, outline = "gold2")
        canvas.itemconfig(three_second, outline = "gold2")
        canvas.itemconfig(three_third, outline = "gold2")
        canvas.itemconfig(three_fourth, outline = "gold2")
        canvas.itemconfig(three_fifth, outline = "gold2")
        master.update()
    elif (data_three < 5.00): 
        canvas.itemconfig(three_first, outline = "red3")
        canvas.itemconfig(three_second, outline = "red3")
        canvas.itemconfig(three_third, outline = "red3")
        canvas.itemconfig(three_fourth, outline = "red3")
        canvas.itemconfig(three_fifth, outline = "red3")
        master.update()
    board.pass_time(0.025)

