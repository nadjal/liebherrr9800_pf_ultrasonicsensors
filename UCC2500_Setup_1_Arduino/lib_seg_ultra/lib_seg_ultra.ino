#include "SevSeg.h" // Load previously added library
SevSeg sevseg;
byte address_response[6];
byte addressrequest[6];
byte resetresponce[6];
byte distancerequest[4];
byte temprequest[4];
byte temp[6];
byte distance[6];
unsigned long startTime = 0;  
int wait = 55;

void setup() 
{
  Serial.begin(19200, SERIAL_8N1); // opens serial port 0, sets data rate to 19200 bps, config (sets data, parity, stopbits; here: (default) data: 8, parity: none, stopbit: 1))  
  Serial1.begin(19200,SERIAL_8N1); // opens serial port 1

  /*Sending Factory Reset*/ 
  byte factoryreset[] ={0xA7, 0x36, 0x55, 0x4F};   //calculated message for factory reset || this message is for ID 7
  Serial1.write(factoryreset, sizeof(factoryreset));
  Serial.print("Factory Reset Sent: ");
  for (byte n=0; n<4; n++)
  {
  Serial.print(factoryreset[n], HEX);
  Serial.print(" ");
  }
  Serial.println("");
  delay(1000);

  /*Recieving Factory Reset*/
   if (Serial1.available()>0)
  {
    Serial.println("Serial Available");
    Serial.print("Response: ");
    for (int n=0; n<6; n++)
    {
    resetresponce[n]=Serial1.read();
     Serial.print(resetresponce[n], HEX);  // displays the response message on the monitor
     Serial.print(" ");
    }
    Serial.println("");
  } 
  

  /*Requesting Sensor Address*/
  byte addressrequest[]={0xA8, 0x00, 0x00, 0x43}; //calculated message for receiving sensor address
  Serial1.write(addressrequest, sizeof(addressrequest));
  Serial.print("Address Request Sent: ");
  for (byte n=0; n<4; n++)
  {
  Serial.print(addressrequest[n], HEX);
  Serial.print(" ");
  }
  Serial.println("");
  delay(1000);
 
  /*Recieving Sensor Address*/
  if (Serial1.available()>0)
  {
    Serial.println("Serial Available");
    Serial.print("Response: ");
    for (int n=0; n<6; n++)
    {
     address_response[n]=Serial1.read();
     Serial.print(address_response[n], HEX);  // displays the response message with the address (at position 4) on the monitor
     Serial.print(" ");
    }
    Serial.println("");
  }
  
  /*Requesting Sensor Temp*/
  byte temprequest[]={0xA7, 0x0A, 0x01, 0x51};  //calculated message for receiving temperature
  Serial1.write(temprequest, sizeof(temprequest));
  Serial.print("Temp Request Sent: ");
  for (byte n=0; n<4; n++)
  {
  Serial.print(temprequest[n], HEX);
  Serial.print(" ");
  }
  Serial.println("");
  delay(1000);
 
  /*Recieving Sensor Temp*/
  if (Serial1.available()>0)
  {
    Serial.println("Serial Available");
    Serial.print("Response: ");
    for (int n=0; n<6; n++)
    {
     temp[n]=Serial1.read();
     Serial.print(temp[n], HEX);  // displays the response message with temperature (at position 4) on the monitor
     Serial.print(" ");
    }
    Serial.println("");
  }

  byte numDigits = 4; // defines the number of digits 
  byte digitPins[] = {10, 11, 12, 13}; // pins to the digits are defined (DIG1, DIG2, DIG3, DIG4)
  byte segmentPins[] = {2,3,4,5,6,7,8,9}; // pins to the segments are defined (A,B,C,D,E,F,G,DP)
  sevseg.begin(COMMON_CATHODE, numDigits, digitPins, segmentPins); // connecting with display 
  sevseg.setBrightness(20); 
}

void loop()
{
  // 55 milliseconds between each measurement || prevents display flickering
  if (millis() > (startTime + wait)) { 
    startTime = millis(); 
    int newdis = receivedis(); // requests a distance measurement at the sensor and returns the measured data
    sevseg.setNumber(newdis); 
  }
    sevseg.refreshDisplay();
}

int receivedis() { 
  byte distancerequest[]={0xAF, 0xFC, 0xFE, 0x40}; //calculated message for receiving distance || profile: C, Measuring cycles: 1, sensor address: 7
  // doesn't work with the UCC4000, outputs 255 every second measurement, "AF FE FE 61" works 
  Serial1.write(distancerequest, sizeof(distancerequest));

  /*Recieving Distance*/
  while (Serial1.available())
  {
     for (int n=0; n<6; n++)
    {
      distance[n]=Serial1.read();
      if (n==4){ 
        Serial.println(distance[n], DEC); // displays the distance in cm on the monitor
      }  
    }
  }
  return (distance[4]); 
}
